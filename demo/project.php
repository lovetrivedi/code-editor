<!doctype html>
<head>
<meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
   <title>Editor</title>
<style type="text/css" media="screen">
.grid6{width:50%;float:left;}
	body{margin:0px;padding:0px;height:100%;}
	*{box-sizing:border-box;}
	.ace_editor {
		position: relative !important;
		border: 1px solid lightgray;
		margin: auto;
		height: calc(33.3333333333% - 34px) !important;
		overflow-y:auto !important;
		width: 100%;
	}
	.ace_scrollbar-v{
		overflow:visible !important;
	}
	.ace_scroller{
		overflow:visible !important;
	}
	.scrollmargin {
		height: 200px;
        text-align: center;
	}
	.editor-container,.editor-result{
		width:50%;
		float:left;
		overflow:hidden;
	}
	.editor-container{
	border-right:1px solid #ccc;
	}
	.editor-result iframe{
		border:none;
		width:100%;
		min-height:calc(100% - 26px);
	}
	h4{
		background:#d5d5d5;
		color:#999;
		font-weight:normal;
		padding:5px;
		font-size:14px;
		margin:0px;
		font-family:arial;
	}
	.dewd{width:40px !important}
	.inwd{width:calc(100% - 40px) !important}
    </style>
	

</head>
<body>



<div class="editor-container">
<button class="hide-code">Hide Code</button>
<div class="html-editor">
<h4>HTML</h4></div>
<pre id="html"></pre>

<div class="css-editor">
<h4>CSS</h4></div>
<pre id="css"></pre>

<div class="js-editor">
<h4>JS</h4></div>
<pre id="js"></pre>

</div>
<div class="editor-result">
<button class="hide-result">Download Zip</button>
<button class="hide-result save-data">Save</button>
<button class="hide-result">Ask For Help</button>
<h4>Result</h4>
<iframe src="data.php" id="FrameID"></iframe>
</div>



</body>
</html>

<script src="../src/ace.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>


var editor = ace.edit("html");
editor.setTheme("ace/theme/tomorrow");
editor.session.setMode("ace/mode/html");
editor.setAutoScrollEditorIntoView(true);
editor.setOption("maxLines", 100);
editor.getSession().setUseWrapMode(true);


var editor2 = ace.edit("css");
editor2.setTheme("ace/theme/tomorrow_night_blue");
editor2.session.setMode("ace/mode/css");
editor2.setAutoScrollEditorIntoView(true);
editor2.setOption("maxLines", 30);
editor2.setOption("minLines", 2);
editor2.getSession().setUseWrapMode(true)

var editor3 = ace.edit("js");
editor3.setTheme("ace/theme/tomorrow");
editor3.session.setMode("ace/mode/javascript");
editor3.setAutoScrollEditorIntoView(true);
editor3.setOption("maxLines", 100);
editor3.getSession().setUseWrapMode(true)


editor.on('blur', function(e) {
	var htmlData = editor.getValue();
	showData();

});
editor2.on('blur', function(e) {
	var cssData = editor2.getValue();
	showData();
});
editor3.on('blur', function(e) {
	var jsData = editor3.getValue();
	showData();
});


</script>
<script>
function showData(){

//getting the values from editors
var htmlData = editor.getValue();
var cssData = editor2.getValue();
var jsData = editor3.getValue();
localStorage.setItem('htmlData', htmlData);
localStorage.setItem('cssData', cssData);
localStorage.setItem('jsData', jsData);

// sending the values in data.php 

 $.post("data.php",
    {
      mydata:htmlData,
	  mycss:cssData,
	  myjs:jsData
    
    },
    function(data,status){
    });

	//reloading the iframe
	
  document.getElementById('FrameID').contentWindow.location.reload(true);
  
  //setHeight function will set the height of gutters. 
  
  setHeight();
}

  //setHeight function will set the height of gutters. this function is called in showData function
function setHeight(){
$('.ace_content').each(function(){
var aceHeight  = $(this).height();
var aceMain = $(this).parents('.ace_editor');
$(aceMain).find('.ace_gutter').height(aceHeight );
});
}

</script>
<script>

//clearing the sessions values
$(window).bind('beforeunload',function(){
<?php 
session_start();
$_SESSION['HTML'] = '';
$_SESSION['css'] = '';
$_SESSION['js'] = '';
?>
});

$(document).ready(function(){
//setting the height of UI
var mainHeight = $(window).height();
$('.editor-container').height(mainHeight); 
$('.editor-result').height(mainHeight); 
//Toggling the view for code (showing and hiding code).
$('.hide-code').click(function(){
	$('.editor-container').toggleClass('dewd');
	$('.editor-result').toggleClass('inwd');
});

var htmlPreData = localStorage.getItem('htmlData');
var cssPreData = localStorage.getItem('cssData');
var jsPreData = localStorage.getItem('jsData');
	if(htmlPreData != ''){
		editor.setValue(localStorage.getItem('htmlData'));
	}
	if(cssPreData != ''){
		editor2.setValue(localStorage.getItem('cssData'));
	}
	if(jsPreData != ''){
		editor3.setValue(localStorage.getItem('jsData'));
	}


$('.save-data').click(function(){
		var htmlData = editor.getValue();
		var cssData = editor2.getValue();
		var jsData = editor3.getValue();
		$.post("handler/save.php",
			{
			  mydata:htmlData,
			  mycss:cssData,
			  myjs:jsData
			
			},
			function(data,status){
			});
	
});
});
</script>
<!--<script src="./show_own_source.js"></script>-->